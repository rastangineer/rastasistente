+++
title = "RastAsistente"
chapter = false
+++

# Hey Rasta

## Tu nuevo RastAsistente

* Realmente _inteligente_ <i class="fas fa-brain"></i>
* Privado <i class="fas fa-user-shield"></i>. No compartimos tu información **con nadie**.
* Seguro <i class="fas fa-shield-alt"></i>
* Multiplataforma:
  * Android <i class="fab fa-android"></i>
  * iOS <i class="fab fa-app-store-ios"></i>
  * Escritorio (Linux <i class="fab fa-linux"></i>, Windows <i class="fab fa-windows"></i>, Mac <i class="fab fa-apple"></i> )
* Compatible con Siri y Google Assistant <i class="fab fa-google"></i>
* [Open Source](https://gitlab.com/rastangineer/) and Free
* API
* SDK
* Hecho con amor <i class="fas fa-hand-peace"></i>

Dile hola a tu nuevo asistente, el **Rastasistente** 💚 💛 ❤️

### Acerca de esta documentación

{{% notice note %}}
Este portal fue generado con GitLab Pages / [Hugo](https://gohugo.io/) and [hugo-theme-learn](https://github.com/matcornic/hugo-theme-learn).
{{% /notice %}}
